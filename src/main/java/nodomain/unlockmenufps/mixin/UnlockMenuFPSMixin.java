package nodomain.unlockmenufps.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import net.minecraft.client.MinecraftClient;

@Mixin(MinecraftClient.class)
public class UnlockMenuFPSMixin {
	@Overwrite
	public int getMaxFramerate() {
		return MinecraftClient.getInstance().options.maxFramerate;
	}
}
